<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Welcome
Route::get('/','IndexController@index')->name('home');

// Get cards
Route::get('/start','IndexController@getCards')->name('start');


// Get next
Route::get('/next','IndexController@getCards')->name('next');


// Get next
Route::get('/game-over','IndexController@gameOver')->name('game-over');