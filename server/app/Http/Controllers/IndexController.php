<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class IndexController extends Controller {

    /*
     * Display the welcome
     *
     */
    public function index(Request $request)
    {
        $request->session()->flush();
        return view( 'index' );
    }

    /**
     *
     * Get card data
     *
     * @param Request $request
     * @return $this
     */
    public function getCards(Request $request)
    {
        //Create client
        $client = new Client();

        // Send request for cards
        // TODO - move to config
        $requestUrl = 'https://cards.davidneal.io/api/cards';

        try {


            if ($request->session()->exists('stage')) { // get stage and cards
                $stage = $request->session()->get('stage');
                $cardData = $request->session()->get('cards');
                $result = false;
                // Evaluate card
                if($this->isCardHigher($cardData[$stage],$cardData[$stage - 1]) && $request->val == 'hi'){
                    echo 'is higher';
                    $result = true;
                }

                if(!$this->isCardHigher($cardData[$stage],$cardData[$stage - 1]) && $request->val == 'lo'){
                    echo 'is higher';
                    $result = true;
                }

                // Increase stage
                if ($result) {
                    $stage = $stage + 1;
                    $request->session()->put('stage', $stage);
                } else{
                    return redirect()->route('game-over');
                }


            } else {
                $stage = 1;
                $request->session()->put('stage', $stage);

                $res = $client->get($requestUrl);

                if ($res->getStatusCode() == 200 ) { // Valid response
                    $cardData = json_decode($res->getBody());
                    //shuffle cards for random order
                    shuffle($cardData);
                    //print_r($cardData);
                    //Store cards in session
                    $request->session()->put('cards', $cardData);
                } else {
                    echo 'Error requesting cards';
                }
            }


            return view( 'card')->with('card',$cardData[$stage - 1]);

        } catch (Exception $e){
            echo 'Error requesting cards';
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function gameOver(Request $request){

        $score = $request->session()->get('stage');
        $request->session()->flush();

        return view( 'result')->with('score',$score);
    }

    /**
     * Evaluate cards
     *
     * @param $cardOne
     * @param $cardTwo
     *
     * @return boolean
     */
    private function isCardHigher($cardOne, $cardTwo){
         return $this->getCardValue($cardOne->value) > $this->getCardValue($cardTwo->value);
    }

    /**
     * interpret card as numerical value
     *
     * @param $value
     * @return int
     */
    private function getCardValue($value){
        if($value == 'J' ){
            return 11;
        }
        if($value == 'Q' ){
            return 12;
        }
        if($value == 'K' ){
            return 13;
        }
        if($value == 'A' ){
            return 1;
        }

        return $value;
    }

}